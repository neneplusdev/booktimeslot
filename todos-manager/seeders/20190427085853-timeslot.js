'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.
    */
      return queryInterface.bulkInsert('timeslots', [
        { time: '9 AM - 10 AM', CreatedAt: new Date(), UpdatedAt: new Date() },
        { time: '10 AM - 11 AM', CreatedAt: new Date(), UpdatedAt: new Date() },
        { time: '11 AM - 12 PM', CreatedAt: new Date(), UpdatedAt: new Date() },
        { time: '12 PM - 1 PM', CreatedAt: new Date(), UpdatedAt: new Date() },
        { time: '1 PM - 2 PM', CreatedAt: new Date(), UpdatedAt: new Date() },
        { time: '2 PM - 3 PM', CreatedAt: new Date(), UpdatedAt: new Date() },
        { time: '3 PM - 4 PM', CreatedAt: new Date(), UpdatedAt: new Date() },
        { time: '4 PM - 5 PM', CreatedAt: new Date(), UpdatedAt: new Date() }
      ], {});
    
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.
    */
      return queryInterface.bulkDelete('timeslot', null, {});
    
  }
};
