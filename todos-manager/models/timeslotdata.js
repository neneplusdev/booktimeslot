'use strict';
module.exports = (sequelize, DataTypes) => {
  const timeslotData = sequelize.define('timeslotData', {
    timeslotId: DataTypes.INTEGER,
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    phoneNumber: DataTypes.STRING
  }, {});
  timeslotData.associate = function(models) {
    // associations can be defined here
  };
  return timeslotData;
};