var express = require('express');
var router = express.Router();
var model = require('../models/index');
 
/* GET todo listing. */
router.get('/', function(req, res, next) {
  model.timeslotData.findAll({})
  .then(timeslotData => res.json({
      error: false,
      data: timeslotData
  }))
  .catch(error => res.json({
      error: true,
      data: [],
      error: error
  }));
});
 
 
/* POST todo. */
router.post('/', function(req, res, next) {
  const {
        timeslotId,
        firstName,
        lastName,
        phoneNumber
    } = req.body;
    model.timeslotData.create({
        timeslotId: timeslotId,
        firstName: firstName,
        lastName: lastName,
        phoneNumber: phoneNumber
    })
    .then(timeslotData => res.status(201).json({
        error: false,
        data: timeslotData,
        message: 'Data in timeslot has been created.'
    }))
    .catch(error => res.json({
        error: true,
        data: [],
        error: error
    }));
});
 
 
/* update todo. */
router.put('/:id', function(req, res, next) {
  const id = req.params.id;
 
    const { firstName, lastName, phoneNumber } = req.body;
 
    model.timeslotData.update({
            firstName: firstName,
            lastName: lastName,
            phoneNumber: phoneNumber
        }, {
            where: {
                id: id
            }
        })
        .then(timeslotData => res.json({
            error: false,
            message: 'Data in timeslot has been updated.'
        }))
        .catch(error => res.json({
            error: true,
            error: error
        }));
});

/* GET todo listing. */
router.get('/:id', function(req, res, next) {
  const id = req.params.id;
  model.timeslotData.findAll({ where: {id: id}})
  .then(timeslotData => res.json({
      error: false,
      data: timeslotData
  }))
  .catch(error => res.json({
      error: true,
      data: [],
      error: error
  }));
});
 
module.exports = router;