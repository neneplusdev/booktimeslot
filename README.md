# BookTimeslot

Book Timeslot

- Clone the repository - https://gitlab.com/neneplusdev/booktimeslot.git

- Go to booktimeslot/todos-manager via terminal

- run command "npm install"

- Create database in mysql with name todos-manager

- Go to folder booktimeslot/todos-manager/config

- Open config.json file

- Change the database name inside development

- Go to booktimeslot/todos-manager via terminal

- Run command "sequelize db:migrate"

- Run command "sequelize db:seed:all"

- Run command "DEBUG=todos-manager:* npm start"

- To run the fronend code create virtual host which should point to booktimeslot/Timeslot

- Run the project by name which you added in virtual host (For me it's timeslot.local)
