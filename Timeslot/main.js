
var mainApp = angular.module("mainApp", ['ngRoute']);

mainApp.config(function($routeProvider) {
	$routeProvider
		.when('/home', {
			templateUrl: 'home.html',
			controller: 'TimeslotController'
		})
		.when('/addtimeslotData/:id', {
			templateUrl: 'timeslotData.html',
			controller: 'TimeslotDataController'
		})
    .when('/editTimeslotData/:id', {
			templateUrl: 'timeslotData.html',
			controller: 'TimeslotDataController'
		})
		.otherwise({
			redirectTo: '/home'
		});
});

mainApp.controller('TimeslotController', function($scope, $http, $window, $routeParams) {
  $scope.timeslot = {};
  $http({
    method : "GET",
    url : "http://localhost:3000/timeslot"
  }).then(function mySuccess(response) {
    $scope.timeslot = response.data.data;
  }, function myError(response) {
    $scope.error = response.statusText;
  });
});

mainApp.controller('TimeslotDataController', function($scope, $http, $window, $routeParams) {
  $scope.user = {};
  $scope.cancel = function() {
    $window.location.href = '#/home';
  };
  if (window.location.href.indexOf("editTimeslotData") > -1) {
    $http({
      method : "GET",
      url : "http://localhost:3000/timeslotdata/" + $routeParams.id
    }).then(function mySuccess(response) {
      console.log('response', response.data.data);
      $scope.user = response.data.data[0];
    }, function myError(response) {
      console.log('response', response);
    });
  }
  
  $scope.save = function() {
    $scope.user.timeslotId = $routeParams.id;
    if (window.location.href.indexOf("editTimeslotData") > -1) {
      $http({
        method : "PUT",
        url : "http://localhost:3000/timeslotdata/" + $routeParams.id,
        data: $scope.user
      }).then(function mySuccess(response) {
        $window.location.href = '#/home';
      }, function myError(response) {
        console.log('response', response);
      });
    } else {
      $http({
        method : "POST",
        url : "http://localhost:3000/timeslotdata",
        data: $scope.user
      }).then(function mySuccess(response) {
        $window.location.href = '#/home';
      }, function myError(response) {
        console.log('response', response);
      });
    }
  };         
});